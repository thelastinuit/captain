FROM gobuffalo/buffalo:v0.12.6 as builder
MAINTAINER thelastinuit

RUN mkdir -p $GOPATH/src/github.com/thelastinuit/captain
WORKDIR $GOPATH/src/github.com/thelastinuit/captain

ADD package.json .
ADD yarn.lock .
RUN yarn install --no-progress
ADD . .
RUN go get $(go list ./... | grep -v /vendor/)
RUN buffalo build --static -o /bin/app

FROM alpine as deployable
RUN apk add --no-cache bash
RUN apk add --no-cache ca-certificates

WORKDIR /bin/

COPY --from=builder /bin/app .

ENV GO_ENV=production

ENV ADDR=0.0.0.0

EXPOSE 3000

CMD /bin/app migrate; /bin/app

CMD exec /bin/app

FROM builder as codable

ENV GO_ENV=development

ENV ADDR=0.0.0.0

EXPOSE 3000
EXPOSE 35729

CMD buffalo dev
