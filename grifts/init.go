package grifts

import (
	"github.com/gobuffalo/buffalo"
	"github.com/thelastinuit/captain/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
