# Captain

## Database Setup
### Create Your Databases

    $ docker-compose run --rm web buffalo db create -a

## Starting the Application

    $ docker-compose up -d web && docker attach captain_web_1

If you point your browser to [http://0.0.0.0:3000](http://0.0.0.0:3000) you should see `Captain`.

**Congratulations!** You now have your Captain application up and running.
